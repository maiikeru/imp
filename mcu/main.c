/*
Michal Jurca , xjurca07
90% zmen, 18.12.2014, upraveno z keyboar a pridany moje funkce
 pro funkcnost hry had
*/


#include <fitkitlib.h>
#include <keyboard/keyboard.h>
#include <thermometer/thermometer.h>

//stavy na zapnuti a vypnuti ledky
#define ON 0
#define OFF 1

// hodnoty pinu
#define PIN0 0x01
#define PIN1 0x02
#define PIN2 0x04
#define PIN3 0x08
#define PIN4 0x10
#define PIN5 0x20
#define PIN6 0x40
#define PIN7 0x80

// negace
#define NPIN0 0xFE
#define NPIN1 0xFD
#define NPIN2 0xFB
#define NPIN3 0xF7
#define NPIN4 0xEF
#define NPIN5 0xDF
#define NPIN6 0xBF
#define NPIN7 0x7F

// nastaveni sloupcu
#define C1(value) (value) ? (P6OUT |= PIN2) : (P6OUT &= NPIN2)
#define C2(value) (value) ? (P6OUT |= PIN6) : (P6OUT &= NPIN6)
#define C3(value) (value) ? (P6OUT |= PIN5) : (P6OUT &= NPIN5)
#define C4(value) (value) ? (P6OUT |= PIN4) : (P6OUT &= NPIN4)
#define C5(value) (value) ? (P1OUT |= PIN6) : (P1OUT &= NPIN6)
#define C6(value) (value) ? (P1OUT |= PIN2) : (P1OUT &= NPIN2)
#define C7(value) (value) ? (P1OUT |= PIN3) : (P1OUT &= NPIN3)
#define C8(value) (value) ? (P1OUT |= PIN7) : (P1OUT &= NPIN7)

//nastaveni radku
#define R1(value) (value) ? (P1OUT &= NPIN4) : (P1OUT |= PIN4)
#define R2(value) (value) ? (P2OUT &= NPIN0) : (P2OUT |= PIN0)
#define R3(value) (value) ? (P2OUT &= NPIN3) : (P2OUT |= PIN3)
#define R4(value) (value) ? (P2OUT &= NPIN4) : (P2OUT |= PIN4)
#define R5(value) (value) ? (P2OUT &= NPIN5) : (P2OUT |= PIN5)
#define R6(value) (value) ? (P2OUT &= NPIN6) : (P2OUT |= PIN6)
#define R7(value) (value) ? (P2OUT &= NPIN7) : (P2OUT |= PIN7)
#define R8(value) (value) ? (P6OUT &= NPIN7) : (P6OUT |= PIN7)

#define CMD_NONE  0
#define CMD_UP    1
#define CMD_DOWN  2
#define CMD_LEFT  3 
#define CMD_RIGHT 4
#define CMD_RESET 5
#define CMD_START 6

//naposledy precteny znak
char last_ch; 

// ulozen had
unsigned char lenghtCobra[15];

// pocet dilku hada
unsigned int sizeCobra;

// nastavi hodnotu na vykresleni ledky
unsigned long long setLed;

// hodnota stiknute klavesy
unsigned int keyPressed;

// vygenerovana pozice v disleji
unsigned int youmy;

// pocet snezenych jidel
unsigned int cntYoumy;

// maska podle cisla LED
#define LED(number) ((unsigned long long)1 << number)

void initializeOutputs()
{
// nastaveni pinu
P1DIR = 0xDC; // 00100011
P2DIR = 0xF9; // 00001001
P3DIR = 0x07; // 00000111
P4DIR = 0x00; // 11111111
P6DIR = 0xF4; // 00001011

// vsechny vystupy do stavu OFF
C1(OFF); C2(OFF); C3(OFF); C4(OFF); C5(OFF); C6(OFF); C7(OFF); C8(OFF);

R1(OFF); R2(OFF); R3(OFF); R4(OFF); R5(OFF); R6(OFF); R7(OFF); R8(OFF);
}


void drawDisplay()
{

C1(ON);
R1((setLed & (LED(0))) ? ON : OFF); R2((setLed & (LED(8))) ? ON : OFF);
R3((setLed & (LED(16))) ? ON : OFF); R4((setLed & (LED(24))) ? ON : OFF);
R5((setLed & (LED(32))) ? ON : OFF); R6((setLed & (LED(40))) ? ON : OFF);
R7((setLed & (LED(48))) ? ON : OFF); R8((setLed & (LED(56))) ? ON : OFF);
delay_ms(1);
C1(OFF);


C2(ON);
R1((setLed & (LED(1))) ? ON : OFF); R2((setLed & (LED(9))) ? ON : OFF);
R3((setLed & (LED(17))) ? ON : OFF); R4((setLed & (LED(25))) ? ON : OFF);
R5((setLed & (LED(33))) ? ON : OFF); R6((setLed & (LED(41))) ? ON : OFF);
R7((setLed & (LED(49))) ? ON : OFF); R8((setLed & (LED(57))) ? ON : OFF);
delay_ms(1);
C2(OFF);

C3(ON);
R1((setLed & (LED(2))) ? ON : OFF); R2((setLed & (LED(10))) ? ON : OFF);
R3((setLed & (LED(18))) ? ON : OFF); R4((setLed & (LED(26))) ? ON : OFF);
R5((setLed & (LED(34))) ? ON : OFF); R6((setLed & (LED(42))) ? ON : OFF);
R7((setLed & (LED(50))) ? ON : OFF); R8((setLed & (LED(58))) ? ON : OFF);
delay_ms(1);
C3(OFF);

C4(ON);
R1((setLed & (LED(3))) ? ON : OFF); R2((setLed & (LED(11))) ? ON : OFF);
R3((setLed & (LED(19))) ? ON : OFF); R4((setLed & (LED(27))) ? ON : OFF);
R5((setLed & (LED(35))) ? ON : OFF); R6((setLed & (LED(43))) ? ON : OFF);
R7((setLed & (LED(51))) ? ON : OFF); R8((setLed & (LED(59))) ? ON : OFF);
delay_ms(1);
C4(OFF);

C5(ON);
R1((setLed & (LED(4))) ? ON : OFF); R2((setLed & (LED(12))) ? ON : OFF);
R3((setLed & (LED(20))) ? ON : OFF); R4((setLed & (LED(28))) ? ON : OFF);
R5((setLed & (LED(36))) ? ON : OFF); R6((setLed & (LED(44))) ? ON : OFF);
R7((setLed & (LED(52))) ? ON : OFF); R8((setLed & (LED(60))) ? ON : OFF);
delay_ms(1);
C5(OFF);

C6(ON);
R1((setLed & (LED(5))) ? ON : OFF); R2((setLed & (LED(13))) ? ON : OFF);
R3((setLed & (LED(21))) ? ON : OFF); R4((setLed & (LED(29))) ? ON : OFF);
R5((setLed & (LED(37))) ? ON : OFF); R6((setLed & (LED(45))) ? ON : OFF);
R7((setLed & (LED(53))) ? ON : OFF); R8((setLed & (LED(61))) ? ON : OFF);
delay_ms(1);
C6(OFF);

C7(ON);
R1((setLed & (LED(6))) ? ON : OFF); R2((setLed & (LED(14))) ? ON : OFF);
R3((setLed & (LED(22))) ? ON : OFF); R4((setLed & (LED(30))) ? ON : OFF);
R5((setLed & (LED(38))) ? ON : OFF); R6((setLed & (LED(46))) ? ON : OFF);
R7((setLed & (LED(54))) ? ON : OFF); R8((setLed & (LED(62))) ? ON : OFF);
delay_ms(1);
C7(OFF);

C8(ON);
R1((setLed & (LED(7))) ? ON : OFF); R2((setLed & (LED(15))) ? ON : OFF);
R3((setLed & (LED(23))) ? ON : OFF); R4((setLed & (LED(31))) ? ON : OFF);
R5((setLed & (LED(39))) ? ON : OFF); R6((setLed & (LED(47))) ? ON : OFF);
R7((setLed & (LED(55))) ? ON : OFF); R8((setLed & (LED(63))) ? ON : OFF);
delay_ms(1);
C8(OFF);

}


/*******************************************************************************
 * Vypis uzivatelske napovedy (funkce se vola pri vykonavani prikazu "help")
*******************************************************************************/
void print_user_help(void)
{
}


/*******************************************************************************
 * Obsluha klavesnice
*******************************************************************************/
int keyboard_idle()
{
  char key = key_decode(read_word_keyboard_4x4());
if (key  != last_ch) // stav se zmnenil
{
  last_ch = key;

  if(key != 0 )// vylucime pusteni klavesy
  {
  
    switch(key)
    {
      case '4':
      keyPressed = CMD_LEFT; 
      term_send_str("CMD_LEFT");
      break;
      
      case '6':
      keyPressed = CMD_RIGHT;
      term_send_str("CMD_RIGHT"); 
      break;

      case '2':
      keyPressed = CMD_UP; 
      term_send_str("CMD_UP");
      break;

      case '8':
      keyPressed = CMD_DOWN;
      term_send_str("CMD_DOWN"); 
      break;

      case '*':
      keyPressed = CMD_START;
      term_send_str("CMD_START"); 
      break;

      case '#':
      keyPressed = CMD_RESET; 
      term_send_str("CMD_RESET");
      break;

      default :
        break;
    }
  }
 } 

  return CMD_NONE;
}



/*******************************************************************************
 * Dekodovani a vykonani uzivatelskych prikazu
*******************************************************************************/
unsigned char decode_user_cmd(char *cmd_ucase, char *cmd)
{
  return CMD_UNKNOWN;
}

/*******************************************************************************
 * Inicializace periferii/komponent po naprogramovani FPGA
*******************************************************************************/
void fpga_initialized()
{
}


void setLEDNum(int num)
{
  setLed |= ((unsigned long long)1 << (num - 1));
}

void drawCobra(){
term_send_num(lenghtCobra[0]);
term_send_char(' ');
  setLed = 0;
  int i;
  for(i = 0; i < sizeCobra; i++){
    setLEDNum(lenghtCobra[i]);
  }
}

void drawSmile()
{
  setLed = 0;

  setLEDNum(18); setLEDNum(22); setLEDNum(27); setLEDNum(28); setLEDNum(29); setLEDNum(43); setLEDNum(45); 
  setLEDNum(26); setLEDNum(30); setLEDNum(10); setLEDNum(14);
  setLEDNum(42); setLEDNum(51); setLEDNum(50); setLEDNum(46); setLEDNum(53); setLEDNum(54);
}

void makeStep(){
  int i;
  for(i = sizeCobra -1; i > 0; i--){
    lenghtCobra[i] = lenghtCobra[i-1];
  }

  // nastaveni pozice
  switch(keyPressed)
  {
    case CMD_UP:
      lenghtCobra[0] += 8;
      break;

    case CMD_DOWN:
      lenghtCobra[0] -= 8;
      break;

    case CMD_LEFT:
      lenghtCobra[0] -= 1;
      break;  

    case CMD_RIGHT:
      lenghtCobra[0] += 1;
      break;

    default:
      lenghtCobra[0] += 1;
      break;
  }
}

unsigned int gen_youmy()
{

  unsigned int coord;
  while(1)
  {
    coord = (unsigned) rand() % 64 +1;

    if (isBody(coord))
    {
      break;
    }
  }

term_send_char(' :');
term_send_num(coord);
term_send_char(': ');


return coord;
}

void drawYoumy()
{
  setLEDNum(youmy);
}

unsigned short isYoumy()
{
  return youmy == lenghtCobra[0];
}

unsigned short isBody(unsigned int num)
{
  int i;

  for ( i = 1; i < sizeCobra; ++i)
  {
    if (lenghtCobra[i] == num)
      return 0;
  }

  return 1;
}

unsigned short inRange()
{
    unsigned int pos = lenghtCobra[0];

    if (pos > 64)
      return 0;

    if (!isBody(pos))
      return 0;

    int i;

    for (i = 8; i < 60; i+= 8)
    {
      if (((lenghtCobra[0] == (i+1)) && (lenghtCobra[1] == i)) || ((lenghtCobra[0] == i) && (lenghtCobra[1] == (i+1)) ))
      return 0;    
    }

    return 1;
}


/*******************************************************************************
 * Hlavni funkce
*******************************************************************************/
int main(void)
{

  thermometer_init_rand();
  unsigned int cnt = 0;
  last_ch = 0;

  initialize_hardware();
  keyboard_init();
  initializeOutputs();

  set_led_d6(1);                       // rozsviceni D6
  set_led_d5(1);                       // rozsviceni D5

  setLed = 0;
  sizeCobra = 3;
  lenghtCobra[0] = 3;
  lenghtCobra[1] = 2;
  lenghtCobra[2] = 1;
  cntYoumy = 0;

  int i;
  unsigned int count = 0;

  youmy = gen_youmy(); 
  makeStep();
  keyPressed = CMD_RESET;

  while (1)
  {
    cnt++;
    if (cnt > 50)
    {
      cnt = 0;
      flip_led_d6();                   // negace portu na ktere je LED
    }

    if (keyPressed == CMD_RESET)
    {
      count = 0;
      setLed = 0;
      sizeCobra = 3;
      lenghtCobra[0] = 2;
      lenghtCobra[1] = 1;
      makeStep();
    }

    if (keyPressed == CMD_START){
      count = 1;
      keyPressed = CMD_RIGHT;
    }

    if (count > 0)
      makeStep();
  
    drawCobra();
    drawYoumy();

    if (isYoumy())
    {
        youmy = gen_youmy();  
        cntYoumy += 1;  
        
        if (cntYoumy == 3)
        {
          cntYoumy = 0;

          if (sizeCobra < 15)
            sizeCobra++;
        }
    }

if (!inRange())
    {
      keyPressed = CMD_RESET;
      int j ;
      drawSmile();
      for ( j = 0; j < 64; ++j)
      {
        drawDisplay();
      }
    } 

for ( i = 0; i < 64; ++i)
    {
      drawDisplay();
      keyboard_idle();
    }
    // obsluha terminalu
    terminal_idle();  
  }      
}
